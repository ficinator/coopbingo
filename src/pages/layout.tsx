import { MenuButtonItem } from '@/components/menuButtonItem';
import { UserDialog } from '@/components/userDialog';
import { auth } from '@/firebase';
import { Menu } from '@headlessui/react';
import {
    ArrowLeftOnRectangleIcon,
    ChevronDownIcon,
    PencilIcon,
    UserCircleIcon,
} from '@heroicons/react/24/solid';
import { signOut } from 'firebase/auth';
import Head from 'next/head';
import Image from 'next/image';
import Link from 'next/link';
import { ReactNode, useState } from 'react';
import logo from '../../public/cb.png';
import { appName, getTitle } from '../utils/text';
import { User } from './api/users';

export default function Layout({
    title,
    currentUser,
    children,
}: {
    title?: string;
    currentUser?: User | null;
    children: ReactNode;
}) {
    const userMenuItems = [
        // <RadioGroup
        //     value={theme}
        //     onChange={setTheme}
        //     className="inline-flex items-center"
        // >
        //     <RadioGroup.Label>{theme}</RadioGroup.Label>
        //     <RadioGroup.Option
        //         value="system"
        //         className="ui-checked:text-primary"
        //     >
        //         <ComputerDesktopIcon className="h-5 w-5" />
        //     </RadioGroup.Option>
        //     <RadioGroup.Option
        //         value="light"
        //         className="ui-checked:text-primary"
        //     >
        //         <SunIcon className="h-5 w-5" />
        //     </RadioGroup.Option>
        //     <RadioGroup.Option value="dark" className="ui-checked:text-primary">
        //         <MoonIcon className="h-5 w-5" />
        //     </RadioGroup.Option>
        // </RadioGroup>,
        <MenuButtonItem
            key="0"
            onClick={() => setIsUserDialogOpen(true)}
            IconType={PencilIcon}
        >
            Edit
        </MenuButtonItem>,
        <MenuButtonItem
            key="1"
            onClick={() => signOut(auth)}
            IconType={ArrowLeftOnRectangleIcon}
        >
            Log Out
        </MenuButtonItem>,
    ];

    const [isUserDialogOpen, setIsUserDialogOpen] = useState(false);

    return (
        <>
            <Head>
                <title>{getTitle(title)}</title>
                <meta name="description" content="Co-operative bingo game" />
                <link rel="icon" href="/favicon.ico" />
            </Head>

            <header className="border-b dark:border-gray-700">
                <div className="mx-auto flex items-center justify-between p-2">
                    <Link
                        href="/"
                        className="group inline-flex items-center p-2 align-middle"
                    >
                        <Image src={logo} alt="CB" className="mr-2 w-10" />
                        <span className="border-y border-y-transparent group-hover:border-b-black dark:group-hover:border-b-white">
                            {appName}
                        </span>
                    </Link>
                    {currentUser ? (
                        <>
                            <Menu as="ul" className="relative z-[1]">
                                <Menu.Button className="inline-flex items-center rounded-md p-2 hover:bg-gray-200 dark:hover:bg-gray-800">
                                    <UserCircleIcon className="mr-2 h-5 w-5" />
                                    {currentUser.name || '???'}
                                    <ChevronDownIcon className="ml-2 h-4 w-4" />
                                </Menu.Button>
                                <Menu.Items className="absolute right-0 mt-1 w-48 min-w-full rounded-md bg-white p-1 shadow-lg dark:bg-gray-800">
                                    {userMenuItems.map(
                                        (userMenuItem, index) => (
                                            <Menu.Item
                                                as="li"
                                                key={index}
                                                className="rounded-md ui-active:bg-teal-100 dark:ui-active:bg-primary"
                                            >
                                                {userMenuItem}
                                            </Menu.Item>
                                        )
                                    )}
                                </Menu.Items>
                            </Menu>

                            <UserDialog
                                open={isUserDialogOpen}
                                onClose={() => setIsUserDialogOpen(false)}
                                currentUser={currentUser}
                            />
                        </>
                    ) : (
                        <div className="m-2 mr-8 inline-flex items-center">
                            <UserCircleIcon className="mr-2 h-5 w-5" />
                            ???
                        </div>
                    )}
                </div>
            </header>

            <main className="mx-auto p-2 sm:p-4">{children}</main>

            <footer className="border-t p-4 text-center text-sm text-gray-500 dark:border-gray-700 dark:text-gray-400">
                <Link
                    href="/"
                    className="mx-auto border-y border-transparent text-center hover:border-b-gray-400 dark:hover:border-b-gray-700"
                >
                    {appName}
                </Link>
            </footer>
        </>
    );
}
