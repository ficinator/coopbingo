import { IconButton } from '@/components/iconButton';
import { LoadingWrapper } from '@/components/loadingWrapper';
import {
    PencilIcon,
    UserCircleIcon,
    UserPlusIcon,
} from '@heroicons/react/24/solid';
import classNames from 'classnames';
import Head from 'next/head';
import { NextRouter, useRouter } from 'next/router';
import { MouseEventHandler, useEffect, useState } from 'react';
import { PlayerDialog } from '../../components/playerDialog';
import { useFields, useGame, usePlayers } from '../../hooks/games';
import { useCurrentUser } from '../../hooks/users';
import {
    checkWin,
    Color,
    getBackgroundClassName,
    getSelectedFieldCount,
    Size,
} from '../../utils/game';
import { getTitle } from '../../utils/text';
import { Game } from '../api/games';
import { Field } from '../api/games/[gameId]/fields';
import { toggleField } from '../api/games/[gameId]/fields/[fieldId]';
import { Player } from '../api/games/[gameId]/players';
import { User } from '../api/users';

export default function GamePage() {
    const router: NextRouter = useRouter();
    const gameId = router.query.gameId as string | undefined;
    const [currentUser] = useCurrentUser();
    const [game, loadingGame] = useGame(gameId);
    const [fields, loadingFields] = useFields(gameId);
    const [players, loadingPlayers] = usePlayers(gameId);

    if (!currentUser || loadingGame || loadingFields || loadingPlayers)
        return <LoadingWrapper>Loading game...</LoadingWrapper>;
    if (!game) return <p>Game with id {gameId} not found.</p>;
    if (!fields) return <p>Game {gameId} has no fields.</p>;

    return (
        <>
            <Head>
                <title>{getTitle(game.title)}</title>
            </Head>
            {checkWin(game.size, fields) && (
                <p className="p-6 text-center">Winner!!!</p>
            )}
            <div className="my-2 flex flex-wrap justify-center gap-4 sm:my-4 lg:gap-6">
                <div className="overflow-x-auto">
                    <CardComponent
                        game={game}
                        fields={fields}
                        currentUserId={currentUser.id}
                        players={players}
                    />
                </div>
                <div className="">
                    <PlayerList
                        game={game}
                        fields={fields}
                        currentUser={currentUser}
                        players={players}
                        router={router}
                    />
                </div>
            </div>
        </>
    );
}

function CardComponent({
    game,
    fields,
    currentUserId,
    players,
}: {
    game: Game;
    fields: Field[];
    currentUserId: string;
    players: Player[] | undefined;
}) {
    const cardClassNames: { [key in Size]: string } = {
        3: 'text-xs grid-rows-[repeat(3,6rem)] grid-cols-[repeat(3,6rem)] md:grid-cols-[repeat(3,7rem)] md:grid-rows-[repeat(3,7rem)] lg:grid-cols-[repeat(3,8rem)] lg:grid-rows-[repeat(3,8rem)]',
        4: 'text-xs grid-cols-[repeat(4,minmax(5rem,6rem))] grid-rows-[repeat(4,6rem)] sm:grid-cols-[repeat(4,6rem)] md:grid-cols-[repeat(4,7rem)] md:grid-rows-[repeat(4,7rem)] lg:grid-cols-[repeat(4,8rem)] lg:grid-rows-[repeat(4,8rem)]',
        5: 'text-[.55rem] grid-cols-[repeat(5,minmax(4rem,5rem))] grid-rows-[repeat(5,5rem)] sm:text-xs sm:grid-cols-[repeat(5,6rem)] sm:grid-rows-[repeat(5,6rem)] md:grid-cols-[repeat(5,7rem)] md:grid-rows-[repeat(5,7rem)] lg:grid-cols-[repeat(5,8rem)] lg:grid-rows-[repeat(5,8rem)]',
        6: 'text-[.5rem] grid-cols-[repeat(6,minmax(3.5rem,5rem))] grid-rows-[repeat(6,5rem)] sm:text-xs sm:grid-cols-[repeat(6,6rem)] sm:grid-rows-[repeat(6,6rem)] md:grid-cols-[repeat(6,7rem)] md:grid-rows-[repeat(6,7rem)] lg:grid-cols-[repeat(6,8rem)] lg:grid-rows-[repeat(6,8rem)]',
        7: 'text-[.45rem] grid-cols-[repeat(7,minmax(3rem,4.5rem))] grid-rows-[repeat(7,4.5rem)] sm:text-[.6rem] sm:grid-cols-[repeat(7,minmax(5rem,5.5rem))] sm:grid-rows-[repeat(7,5.5rem)] md:grid-cols-[repeat(7,7rem)] md:grid-rows-[repeat(7,7rem)] lg:grid-cols-[repeat(7,8rem)] lg:grid-rows-[repeat(7,8rem)]',
    };
    return (
        <div
            className={`grid md:text-sm lg:text-base ${
                cardClassNames[game.size]
            }`}
        >
            {fields.map((field, index: number) =>
                field ? (
                    <FieldComponent
                        key={index}
                        gameId={game.id}
                        field={field}
                        currentUserId={currentUserId}
                        players={players}
                    />
                ) : null
            )}
        </div>
    );
}

function FieldComponent({
    gameId,
    field,
    currentUserId,
    players,
}: {
    gameId: string;
    field: Field;
    currentUserId: string;
    players: Player[] | undefined;
}) {
    const fieldId: string = field.id;
    const selectedByUsers = field.selectedByUsers || [];

    const onClick = async (): Promise<void> => {
        if (field.isJoker) return;

        try {
            await toggleField(gameId, fieldId, currentUserId, selectedByUsers);
        } catch (error) {
            console.error(error);
        }
    };

    const fieldColors = getFieldColors(players, selectedByUsers);

    return (
        <button
            key={fieldId}
            className={classNames({
                'relative m-0.5 flex items-center justify-center overflow-hidden rounded-md p-1 lg:rounded-lg':
                    true,
                'cursor-default bg-black text-white dark:bg-white dark:text-black':
                    field.isJoker,
                'bg-gray-200 hover:bg-gray-100 dark:bg-gray-700 dark:hover:bg-gray-600':
                    !field.isJoker && selectedByUsers.length === 0,
                [getBackgroundClassName(fieldColors)]:
                    selectedByUsers.length > 0,
            })}
            onClick={onClick}
        >
            {field.text}
            {selectedByUsers.length > 1 && (
                <span className="absolute bottom-1 right-1 h-4 w-4 rounded-full bg-gray-200 leading-4 text-black dark:bg-gray-700 dark:text-white sm:bottom-1.5 sm:right-1.5 sm:h-5 sm:w-5 sm:leading-5 lg:bottom-2.5 lg:right-2.5 lg:h-6 lg:w-6 lg:leading-6">
                    {selectedByUsers.length}
                </span>
            )}
        </button>
    );
}

function getFieldColors(
    players: Player[] | undefined,
    selectedByUsers: string[]
): (Color | undefined)[] {
    return selectedByUsers.map(
        (userId) => players?.find((player) => player.userId === userId)?.color
    );
}

function PlayerList({
    game,
    fields,
    currentUser,
    players,
    router,
}: {
    game: Game;
    fields: Field[];
    currentUser: User;
    players: Player[] | undefined;
    router: NextRouter;
}) {
    const currentUserId = currentUser.id;
    const [urlCopiedStatus, setUrlCopiedStatus] = useState<'ok' | 'err'>();
    const [isPlayerDialogOpen, setIsPlayerDialogOpen] = useState(false);
    const [playersSorted, setPlayersSorted] =
        useState<(Player & { fieldCount: number })[]>();

    useEffect(() => {
        if (!players?.find((player) => player.userId === currentUserId)) {
            setIsPlayerDialogOpen(true);
            // return;
        }

        setPlayersSorted(
            players
                ?.map((player) => ({
                    ...player,
                    fieldCount: getSelectedFieldCount(fields, player.userId),
                }))
                .sort(
                    (a, b) =>
                        b.fieldCount - a.fieldCount ||
                        a.name.localeCompare(b.name)
                )
        );
    }, [currentUserId, fields, players]);

    async function handleInvite() {
        try {
            await navigator.clipboard.writeText(
                `${location.origin}${router.asPath}`
            );
            setUrlCopiedStatus('ok');
        } catch (error) {
            setUrlCopiedStatus('err');
        } finally {
            setTimeout(() => setUrlCopiedStatus(undefined), 1500);
        }
    }

    return (
        <aside className="max-w-sm">
            {game.title && <h2 className="text-xl font-bold">{game.title}</h2>}
            {game.description && (
                <p className="text-sm text-gray-400 dark:text-gray-500">
                    {game.description}
                </p>
            )}
            <div className="my-3 flex items-center justify-between gap-4">
                <h3>
                    {players?.length
                        ? `${players.length} Player${
                              players.length > 1 ? 's' : ''
                          }`
                        : 'No Players'}
                </h3>
                <div className="relative">
                    <IconButton
                        onClick={handleInvite}
                        IconType={UserPlusIcon}
                        className="inline-flex items-center rounded-md bg-primary px-3 py-2 text-white hover:bg-primary-light"
                    >
                        Invite
                    </IconButton>
                    {urlCopiedStatus && (
                        <p
                            className={classNames({
                                "absolute mt-2 w-full rounded-md p-2 text-center text-sm shadow-md before:absolute before:left-1/2 before:-top-4 before:-ml-2 before:border-8 before:border-transparent before:content-['']":
                                    true,
                                'bg-green-100 before:border-b-green-100 dark:bg-green-800 dark:before:border-b-green-800':
                                    urlCopiedStatus === 'ok',
                                'bg-red-100 before:border-b-red-100 dark:bg-red-800 dark:before:border-b-red-800':
                                    urlCopiedStatus === 'err',
                            })}
                        >
                            {urlCopiedStatus === 'ok'
                                ? 'Game link copied.'
                                : "Cannot copy the game link. Please, do it manually from the browser's address bar."}
                        </p>
                    )}
                </div>
            </div>
            {playersSorted?.length ? (
                <ul>
                    {playersSorted.map((player) => (
                        <PlayerComponent
                            key={player.id}
                            player={player}
                            onEditPlayerClick={
                                player.userId === currentUserId
                                    ? () => setIsPlayerDialogOpen(true)
                                    : undefined
                            }
                        />
                    ))}
                </ul>
            ) : (
                <p className="m-4 text-center text-gray-300 dark:text-gray-600">
                    No players here
                </p>
            )}

            <PlayerDialog
                open={isPlayerDialogOpen}
                onClose={() => setIsPlayerDialogOpen(false)}
                game={game}
                currentUser={currentUser}
                players={players}
            />
        </aside>
    );
}

function PlayerComponent({
    player,
    onEditPlayerClick,
}: {
    player: Player & { fieldCount: number };
    onEditPlayerClick?: MouseEventHandler<HTMLButtonElement>;
}) {
    const isCurrentPlayer = !!onEditPlayerClick;
    const backgroundClassName = getBackgroundClassName([player.color]);
    return (
        <li
            className={classNames({
                'flex items-center gap-3 rounded-md p-2': true,
                [backgroundClassName]: isCurrentPlayer,
            })}
        >
            <div className="flex grow items-center">
                {isCurrentPlayer && <UserCircleIcon className="mr-2 h-4 w-4" />}
                <span
                    className={classNames({
                        'ml-6': !isCurrentPlayer,
                    })}
                >
                    {player.name}
                </span>
                {isCurrentPlayer && (
                    <button
                        onClick={onEditPlayerClick}
                        className="ml-2 rounded-md p-2 hover:bg-white/30"
                    >
                        <PencilIcon className="h-4 w-4" />
                        <span className="sr-only">Edit Player</span>
                    </button>
                )}
            </div>
            <div
                className={classNames({
                    'h-6 w-6 rounded-full text-center text-sm leading-6': true,
                    // 'bg-white text-black dark:bg-black dark:text-white':
                    //     isCurrentPlayer,
                    [backgroundClassName]: !isCurrentPlayer,
                })}
            >
                {player.fieldCount}
            </div>
        </li>
    );
}
