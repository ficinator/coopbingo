import {
    collection,
    DocumentData,
    FirestoreDataConverter,
    getDocs,
    orderBy,
    query,
    QueryDocumentSnapshot,
    where,
    WithFieldValue,
} from 'firebase/firestore';
import type { NextApiRequest, NextApiResponse } from 'next';
import { db } from '../../../firebase';

export type Example = {
    id: string;
    text: string;
};

export type Template = {
    id: string;
    name: string;
    emoji?: string;
    examples?: Example[];
};

/**
 * Converts Template <-> firebase document data.
 */
export const templateConverter: FirestoreDataConverter<Template> = {
    toFirestore(template: WithFieldValue<Template>): DocumentData {
        return template;
    },
    fromFirestore(snapshot: QueryDocumentSnapshot): Template {
        const data = snapshot.data();
        return {
            id: snapshot.id,
            name: data.name,
            emoji: data.emoji || null,
        };
    },
};

/**
 * Converts Item <-> firebase document data.
 */
export const exampleConverter: FirestoreDataConverter<Example> = {
    toFirestore(example: WithFieldValue<Example>): DocumentData {
        return {
            text: example.text,
        };
    },
    fromFirestore(snapshot: QueryDocumentSnapshot): Example {
        const data = snapshot.data();
        return {
            id: snapshot.id,
            text: data.text,
        };
    },
};

export const templatesRef = collection(db, 'templates').withConverter(
    templateConverter
);

export async function getTemplates(): Promise<Template[]> {
    const templatesQuery = query(
        templatesRef,
        where('isPublic', '==', true),
        orderBy('name')
    );
    const templatesSnapshot = await getDocs(templatesQuery);
    const templates: Template[] = templatesSnapshot.docs.map(
        (templateSnapshot) => templateSnapshot.data()
    );
    console.log(`Got templates ${templates.map((template) => template.name)}`);
    return templates;
}

export async function getTemplateIds(): Promise<string[]> {
    return (await getTemplates()).map((template) => template.id);
}

export default async function handler(
    req: NextApiRequest,
    res: NextApiResponse<Template[]>
) {
    res.status(200).json(await getTemplates());
}
