import { doc, getDoc } from 'firebase/firestore';
import type { NextApiRequest, NextApiResponse } from 'next';
import { Template, templatesRef } from '..';
import { getTemplateExamples } from './examples';

export async function getTemplate(templateId: string): Promise<Template> {
    const templateRef = doc(templatesRef, templateId);
    const templateSnapshot = await getDoc(templateRef);
    if (!templateSnapshot.exists()) {
        throw Error(`Template with id "${templateId}" not found.`);
    }
    const template = templateSnapshot.data();
    console.log(`Got template ${template.name}`);
    return template;
}

export async function getTemplateWithExamples(
    templateId: string
): Promise<Template> {
    const template: Template = await getTemplate(templateId);
    template.examples = await getTemplateExamples(templateId);
    return template;
}

export default async function handler(
    req: NextApiRequest,
    res: NextApiResponse<Template | { error: string }>
) {
    try {
        const template: Template | null = await getTemplateWithExamples(
            req.query.templateId as string
        );
        res.status(200).json(template);
    } catch (error) {
        res.status(404).json({ error: (error as Error).message });
    }
}
