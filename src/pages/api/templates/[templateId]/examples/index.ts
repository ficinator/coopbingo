import { collection, doc, getDocs } from 'firebase/firestore';
import type { NextApiRequest, NextApiResponse } from 'next';
import { Example, exampleConverter, templatesRef } from '../..';

export async function getTemplateExamples(
    templateId: string
): Promise<Example[]> {
    const templateRef = doc(templatesRef, templateId);
    const examplesRef = collection(templateRef, 'examples').withConverter(
        exampleConverter
    );
    const examplesSnapshot = await getDocs(examplesRef);
    const examples: Example[] = examplesSnapshot.docs.map((exampleSnapshot) =>
        exampleSnapshot.data()
    );
    return examples;
}

export default async function handler(
    req: NextApiRequest,
    res: NextApiResponse<Example[]>
) {
    res.status(200).json(
        await getTemplateExamples(req.query.templateId as string)
    );
}
