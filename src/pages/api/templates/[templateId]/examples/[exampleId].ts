import { collection, doc, getDoc } from 'firebase/firestore';
import type { NextApiRequest, NextApiResponse } from 'next';
import { Example, exampleConverter, templatesRef } from '../..';

async function getTemplateExample(
    templateId: string,
    exampleId: string
): Promise<Example> {
    const templateRef = doc(templatesRef, templateId);
    const examplesRef = collection(templateRef, 'examples').withConverter(
        exampleConverter
    );
    const exampleRef = doc(examplesRef, exampleId);

    const exampleSnapshot = await getDoc(exampleRef);

    if (!exampleSnapshot.exists()) {
        throw Error(`Example ${exampleRef.path} does not exist.`);
    }

    const example = exampleSnapshot.data();
    return example;
}

export default async function handler(
    req: NextApiRequest,
    res: NextApiResponse<Example | { error: string }>
) {
    try {
        const example = await getTemplateExample(
            req.query.templateId as string,
            req.query.exampleId as string
        );
        res.status(200).json(example);
    } catch (error) {
        return res.status(404).json({ error: (error as Error).message });
    }
}
