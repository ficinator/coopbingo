import {
    arrayRemove,
    arrayUnion,
    doc,
    DocumentReference,
    getDoc,
    UpdateData,
    updateDoc,
} from 'firebase/firestore';
import type { NextApiRequest, NextApiResponse } from 'next';
import { Field, getFieldsRef } from '.';

function getFieldRef(
    gameId: string,
    fieldId: string
): DocumentReference<Field> {
    const fieldsRef = getFieldsRef(gameId);
    const fieldRef = doc(fieldsRef, fieldId);
    return fieldRef;
}

async function getField(gameId: string, fieldId: string): Promise<Field> {
    const gameFieldRef = getFieldRef(gameId, fieldId);
    const fieldSnapshot = await getDoc(gameFieldRef);

    if (!fieldSnapshot.exists()) {
        throw Error(
            `Field with id "${fieldId}" does not exist in game with id "${gameId}".`
        );
    }

    return fieldSnapshot.data();
}

async function updateField(
    gameId: string,
    fieldId: string,
    data: UpdateData<Field>
): Promise<Field> {
    const gameFieldRef = getFieldRef(gameId, fieldId);
    await updateDoc(gameFieldRef, data);
    return getField(gameId, fieldId);
}

export async function toggleField(
    gameId: string,
    fieldId: string,
    userId: string,
    selectedByUsers: string[]
) {
    const fieldRef = getFieldRef(gameId, fieldId);

    const updateFc = selectedByUsers.includes(userId)
        ? arrayRemove
        : arrayUnion;
    await updateDoc(fieldRef, 'selectedByUsers', updateFc(userId));
}

export default async function handler(
    req: NextApiRequest,
    res: NextApiResponse<Field | { error: string }>
) {
    const query = req.query;
    const gameId = query.gameId as string;
    const fieldId = query.fieldId as string;
    const body = req.body;

    switch (req.method) {
        case 'PATCH':
            try {
                const field = await updateField(gameId, fieldId, body);
                res.status(200).json(field);
            } catch (error) {
                res.status(404).json({ error: (error as Error).message });
            }
            break;
        default:
            try {
                const field = await getField(gameId, fieldId);
                res.status(200).json(field);
            } catch (error) {
                res.status(404).json({ error: (error as Error).message });
            }
    }
}
