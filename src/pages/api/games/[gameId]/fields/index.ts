import {
    collection,
    CollectionReference,
    DocumentData,
    FirestoreDataConverter,
    QueryDocumentSnapshot,
    WithFieldValue,
} from 'firebase/firestore';
import { getGameRef } from '..';

export type Field = {
    id: string;
    text: string;
    selectedByUsers?: string[];
    isJoker?: boolean;
};

/**
 * Converts Field <-> firebase document data.
 */
export const fieldConverter: FirestoreDataConverter<Field> = {
    toFirestore(field: WithFieldValue<Field>): DocumentData {
        return field;
    },
    fromFirestore(snapshot: QueryDocumentSnapshot): Field {
        const data = snapshot.data();
        return {
            id: snapshot.id,
            text: data.text,
            selectedByUsers: data.selectedByUsers || [],
            isJoker: data.isJoker,
        };
    },
};

export function getFieldsRef(gameId: string): CollectionReference<Field> {
    const gameRef = getGameRef(gameId);
    const fieldsRef = collection(gameRef, 'fields').withConverter(
        fieldConverter
    );
    return fieldsRef;
}
