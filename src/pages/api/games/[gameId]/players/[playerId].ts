import {
    doc,
    DocumentReference,
    UpdateData,
    updateDoc,
} from 'firebase/firestore';
import { getPlayersRef, Player } from '.';

function getPlayerRef(
    gameId: string,
    playerId: string
): DocumentReference<Player> {
    const playersRef = getPlayersRef(gameId);
    const playerRef = doc(playersRef, playerId);
    return playerRef;
}

export async function updatePlayer(
    gameId: string,
    playerId: string,
    data: UpdateData<Player>
) {
    const playerRef = getPlayerRef(gameId, playerId);
    await updateDoc(playerRef, data);
}
