import {
    addDoc,
    collection,
    DocumentData,
    FirestoreDataConverter,
    QueryDocumentSnapshot,
    WithFieldValue,
} from 'firebase/firestore';
import { getGameRef } from '..';
import { Color } from '../../../../../utils/game';

export type Player = {
    id: string;
    userId: string;
    name: string;
    color?: Color;
};

/**
 * Converts Player <-> firebase document data.
 */
export const playerConverter: FirestoreDataConverter<Player> = {
    toFirestore(player: WithFieldValue<Player>): DocumentData {
        return player;
    },
    fromFirestore(snapshot: QueryDocumentSnapshot): Player {
        const data = snapshot.data();
        return {
            id: snapshot.id,
            userId: data.userId,
            name: data.name,
            color: data.color,
        };
    },
};

export function getPlayersRef(gameId: string) {
    const gameRef = getGameRef(gameId);
    const playersRef = collection(gameRef, 'players').withConverter(
        playerConverter
    );
    return playersRef;
}

export async function addPlayer(
    gameId: string,
    data: Omit<Player, 'id'>
): Promise<string> {
    const playersRef = getPlayersRef(gameId);
    const playerRef = await addDoc(playersRef, data);
    return playerRef.id;
}
