import { doc, DocumentReference, getDoc } from 'firebase/firestore';
import type { NextApiRequest, NextApiResponse } from 'next';
import { Game, gamesRef } from '..';

export function getGameRef(gameId: string): DocumentReference<Game> {
    return doc(gamesRef, gameId);
}

export async function getGame(gameId: string): Promise<Game> {
    const gameRef = getGameRef(gameId);
    const gameSnapshot = await getDoc(gameRef);

    if (!gameSnapshot.exists()) {
        throw Error(`Game with id "${gameId}" not found.`);
    }

    const game: Game = gameSnapshot.data();

    return game;
}

export default async function handler(
    req: NextApiRequest,
    res: NextApiResponse<Game>
) {
    res.status(200).json(await getGame(req.query.gameId as string));
}
