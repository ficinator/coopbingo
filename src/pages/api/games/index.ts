import {
    addDoc,
    collection,
    doc,
    DocumentData,
    FirestoreDataConverter,
    QueryDocumentSnapshot,
    UpdateData,
    updateDoc,
    WithFieldValue,
    writeBatch,
} from 'firebase/firestore';
import type { NextApiRequest, NextApiResponse } from 'next';
import { db } from '../../../firebase';
import { defaultGameSize, Size } from '../../../utils/game';
import { getGame, getGameRef } from './[gameId]';
import { Field, getFieldsRef } from './[gameId]/fields';

export type Game = {
    id: string;
    createdAt: Date;
    size: Size;
    ownerId: string;
    title?: string;
    description?: string;
};

/**
 * Converts Game <-> firebase document data.
 */
export const gameConverter: FirestoreDataConverter<Game> = {
    toFirestore(game: WithFieldValue<Game>): DocumentData {
        return game;
    },
    fromFirestore(snapshot: QueryDocumentSnapshot): Game {
        const data = snapshot.data();
        return {
            id: snapshot.id,
            createdAt: data.createdAt,
            size: data.size,
            ownerId: data.ownerId,
            title: data.title,
            description: data.description,
        };
    },
};

export const gamesRef = collection(db, 'games').withConverter(gameConverter);

export async function createGame(
    size: Size = defaultGameSize,
    ownerId: string,
    fields: { text: string }[],
    title?: string,
    description?: string
): Promise<Game> {
    const game: Omit<Game, 'id'> = {
        createdAt: new Date(),
        ownerId,
        size,
        title,
        description,
    };
    const gameRef = await addDoc(gamesRef, game);

    // add fields collection
    const batch = writeBatch(db);
    const fieldsRef = getFieldsRef(gameRef.id);
    for (let i = 0; i < Math.min(size * size, fields.length); i++) {
        const id = i.toString().padStart(2, '0');
        const text = fields[i].text;
        const field: WithFieldValue<Field> = {
            id,
            text,
            ...(text === 'JOKER' && { isJoker: true }),
        };
        const fieldRef = doc(fieldsRef, id);
        batch.set(fieldRef, field);
    }

    await batch.commit();

    return await getGame(gameRef.id);
}

export async function updateGame(gameId: string, data: UpdateData<Game>) {
    const gameRef = getGameRef(gameId);
    await updateDoc(gameRef, data);
}

export default async function handler(
    req: NextApiRequest,
    res: NextApiResponse<Game | { error: string }>
) {
    const body = req.body;

    switch (req.method) {
        case 'POST':
            try {
                const game: Game = await createGame(
                    body.size,
                    body.ownerId,
                    body.fields,
                    body.title,
                    body.description
                );
                res.status(200).json(game);
            } catch (error) {
                res.status(404).json({ error: (error as Error).message });
            }
            break;

        default:
            res.status(404).json({
                error: `Method ${req.method} not allowed for ${req.query}.`,
            });
    }
}
