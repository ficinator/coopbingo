import { doc, getDoc, setDoc, UpdateData, updateDoc } from 'firebase/firestore';
import { User, usersRef } from '.';

function getUserRef(userId: string) {
    return doc(usersRef, userId);
}

export async function getUser(userId: string): Promise<User> {
    const userRef = getUserRef(userId);
    const userSnapshot = await getDoc(userRef);
    if (!userSnapshot.exists()) {
        throw Error(`User with id "${userId}" not found.`);
    }
    const user = userSnapshot.data();
    return user;
}

export async function setUser(userId: string) {
    const userRef = getUserRef(userId);
    await setDoc(userRef, {
        id: userId,
    });
}

export async function updateUser(userId: string, data: UpdateData<User>) {
    const userRef = getUserRef(userId);
    await updateDoc(userRef, data);
}
