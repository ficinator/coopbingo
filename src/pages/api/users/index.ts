import {
    collection,
    doc,
    DocumentData,
    DocumentReference,
    FirestoreDataConverter,
    getDocs,
    query,
    QueryConstraint,
    QueryDocumentSnapshot,
    where,
    WithFieldValue,
} from 'firebase/firestore';
import { db } from '../../../firebase';

export type User = {
    id: string;
    name?: string;
};

/**
 * Converts User <-> firebase document data.
 */
export const userConverter: FirestoreDataConverter<User> = {
    toFirestore(user: WithFieldValue<User>): DocumentData {
        return user;
    },
    fromFirestore(snapshot: QueryDocumentSnapshot): User {
        const data = snapshot.data();
        return {
            id: snapshot.id,
            name: data.name,
        };
    },
};

export const usersRef = collection(db, 'users').withConverter(userConverter);

export function getUserRef(userId: string): DocumentReference<User> {
    return doc(usersRef, userId);
}

export async function getUsers(userIds?: string[]): Promise<User[]> {
    const queryConstraints: QueryConstraint[] = [];
    if (userIds) {
        queryConstraints.push(where('id', 'in', userIds));
    }
    const usersQuery = query(usersRef, ...queryConstraints);
    const usersSnapshot = await getDocs(usersQuery);
    const users: User[] = usersSnapshot.docs.map((userSnapshot) =>
        userSnapshot.data()
    );
    return users;
}
