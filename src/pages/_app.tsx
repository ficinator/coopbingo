import { signInAnonymously, UserCredential } from 'firebase/auth';
import { NextPage } from 'next';
import type { AppProps } from 'next/app';
import { ReactElement, ReactNode, useEffect } from 'react';
import { SWRConfig } from 'swr';
import { auth } from '../firebase';
import { useCurrentUser } from '../hooks/users';
import '../styles/globals.css';
import Layout from './layout';

type NextPageWithLayout<P = unknown, IP = P> = NextPage<P, IP> & {
    getLayout?: (page: ReactElement) => ReactNode;
};

type AppPropsWithLayout = AppProps & {
    Component: NextPageWithLayout;
};

const fetcher = (resource: RequestInfo, init?: RequestInit) =>
    fetch(resource, init).then((res) => res.json());

async function signIn(): Promise<string> {
    // await setPersistence(auth, browserSessionPersistence);
    const userCredential: UserCredential = await signInAnonymously(auth);
    const user = userCredential.user;
    return user.uid;
}

export default function App({ Component, pageProps }: AppPropsWithLayout) {
    const [currentUser] = useCurrentUser();

    useEffect(() => {
        if (currentUser) {
            console.log(`User with id ${currentUser.id} already signed in.`);
        } else if (currentUser === null) {
            signIn().then((userId) =>
                console.log(`Signed in user with id ${userId}.`)
            );
        }
    }, [currentUser]);

    const getLayout = Component.getLayout ?? ((page) => page);

    return (
        <SWRConfig value={{ fetcher }}>
            <Layout title={pageProps.template?.name} currentUser={currentUser}>
                {getLayout(<Component {...pageProps} />)}
            </Layout>
        </SWRConfig>
    );
}
