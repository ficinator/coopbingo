import { GetStaticPaths, GetStaticProps } from 'next';
import { ReactElement } from 'react';
import { getTemplateIds, Example, Template } from '../api/templates';
import { getTemplateWithExamples } from '../api/templates/[templateId]';
import TemplatesLayout from './layout';

export const getStaticPaths: GetStaticPaths = async () => {
    const templateIds: string[] = await getTemplateIds();
    return {
        paths: templateIds.map((templateId) => ({ params: { templateId } })),
        // returns 404 if the path is not among the static paths
        fallback: false,
    };
};

export const getStaticProps: GetStaticProps = async ({ params }) => {
    const template = await getTemplateWithExamples(
        params?.templateId as string
    );
    return {
        props: {
            template,
        },
    };
};

export default function TemplatePage({ template }: { template: Template }) {
    return (
        <article>
            <h1>Template: {template.name}</h1>
            <small>ID: {template.id}</small>

            <h2>Examples</h2>
            {template.examples?.length ? (
                <ul>
                    {template.examples.map((example: Example) => (
                        <li key={example.id}>{example.text}</li>
                    ))}
                </ul>
            ) : (
                <p>The template has no examples</p>
            )}
        </article>
    );
}

TemplatePage.getLayout = (page: ReactElement) => (
    <TemplatesLayout>{page}</TemplatesLayout>
);
