import Link from 'next/link';
import { NextRouter, useRouter } from 'next/router';
import { ReactNode } from 'react';
import useSWR from 'swr';
import { Game } from '../api/games';
import { Template } from '../api/templates';
import styles from './layout.module.scss';

async function generateGame(templateId: string): Promise<Game> {
    const response = await fetch('/api/games', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({ templateId }),
    });
    return await response.json();
}

export default function TemplatesLayout({ children }: { children: ReactNode }) {
    const { data: templates } = useSWR<Template[]>('/api/templates');
    const router: NextRouter = useRouter();

    return (
        <>
            <h1>Templates</h1>

            <div className={styles['list-detail']}>
                <div className={styles.list}>
                    <ul>
                        {templates?.map(template => (
                            <li
                                key={template.id}
                            >
                                <Link href={`/templates/${template.id}`}>{template.name}</Link>
                                <button onClick={async () => {
                                    const game = await generateGame(template.id);
                                    // route to the game url
                                    router.push(`/games/${game.id}`);
                                }}>+</button>
                            </li>
                        ))}
                    </ul>
                </div>

                <div className={styles.detail}>
                    { children }
                </div>
            </div>
        </>
    );
}