import { ReactElement } from 'react';
import TemplatesLayout from './layout';

export default function TemplatesPage() {
    return <p>No template selected</p>;
}

TemplatesPage.getLayout = (page: ReactElement) => (
    <TemplatesLayout>{page}</TemplatesLayout>
);
