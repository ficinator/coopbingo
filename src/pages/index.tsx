import { ErrorMessage } from '@/components/errorMessage';
import { IconButton } from '@/components/iconButton';
import { LoadingWrapper } from '@/components/loadingWrapper';
import { OptionComponent } from '@/components/option';
import { ShowMoreWrapper } from '@/components/showMoreWrapper';
import { PlayIcon, PlusIcon, TrashIcon } from '@heroicons/react/24/solid';
import {
    FieldArray,
    FieldInputProps,
    Form,
    Formik,
    FormikErrors,
    FormikProps,
    FormikTouched,
} from 'formik';
import { GetStaticProps } from 'next';
import { useRouter } from 'next/router';
import { useEffect, useState } from 'react';
import * as Yup from 'yup';
import { Input } from '../components/input';
import { Label, LabelAbove } from '../components/label';
import { ListboxSelect } from '../components/listbox';
import { SubmitButton } from '../components/submitButton';
import { SwitchCheckbox } from '../components/switch';
import { useExamples } from '../hooks/templates';
import { useCurrentUser } from '../hooks/users';
import { shuffle } from '../utils/array';
import { defaultGameSize, getFields, Size, sizes } from '../utils/game';
import { createGame, Game } from './api/games';
import { getTemplates, Template } from './api/templates';

type Values = {
    templateId: string | undefined;
    size: Size;
    title: string;
    description: string;
    shuffle: boolean;
    joker: boolean;
    fields: { text: string }[];
};

async function generateGame({
    size,
    title,
    description,
    shuffle: doShuffle,
    joker,
    fields,
    ownerId,
}: Values & { ownerId: string }): Promise<Game> {
    const shuffledFields = doShuffle ? shuffle(fields) : fields;
    const numFields = size * size;
    const slicedFields = shuffledFields.slice(0, numFields);
    if (size % 2 !== 0 && joker) {
        slicedFields[Math.floor(numFields / 2)].text = 'JOKER';
    }
    const game = await createGame(
        size,
        ownerId,
        slicedFields,
        title,
        description
    );
    return game;
}

export const getStaticProps: GetStaticProps = async () => {
    const templates = await getTemplates();
    return {
        props: {
            templates,
        },
    };
};

export default function Home({ templates }: { templates: Template[] }) {
    const router = useRouter();
    const [user] = useCurrentUser();

    const defaultTemplateId = '2b19vTBaGBN4G9FZKsTj'; // Bahn

    const initialValues: Values = {
        templateId: defaultTemplateId,
        size: defaultGameSize,
        title: '',
        description: '',
        shuffle: true,
        joker: true,
        fields: getFields(defaultGameSize),
    };

    const validationSchema = Yup.object({
        fields: Yup.array()
            .of(
                Yup.object({
                    text: Yup.string().trim().required('field is required'),
                })
            )
            .test(
                'enough-fields',
                'at least ${minLength} fields are required',
                (fields, { parent: { size }, createError }) => {
                    const length = fields?.length || 0;
                    const minLength = size * size;
                    return (
                        length >= minLength ||
                        createError({ params: { length, minLength } })
                    );
                }
            ),
    });

    async function onSubmit(values: Values) {
        if (!user) {
            console.error('Cannot create game: user not authenticated.');
            return;
        }

        try {
            const game = await generateGame({
                ...values,
                ownerId: user.id,
            });
            router.push(`/games/${game.id}`);
        } catch (error) {
            console.error(error);
        }
    }

    return (
        <div className="mx-auto max-w-6xl">
            <div className="mx-2">
                <Formik
                    initialValues={initialValues}
                    validationSchema={validationSchema}
                    onSubmit={onSubmit}
                >
                    {(props) => <GameForm templates={templates} {...props} />}
                </Formik>
            </div>
        </div>
    );
}

function GameForm({
    templates,
    values,
    touched,
    dirty,
    errors,
    isSubmitting,
    getFieldProps,
    setFieldValue,
}: { templates: Template[] } & FormikProps<Values>) {
    const { templateId, size } = values;
    const { examples, error: examplesError } = useExamples(templateId);
    const [showAllFields, setShowAllFields] = useState(false);

    useEffect(() => {
        setFieldValue('fields', getFields(size, examples));
    }, [examples, size, setFieldValue]);

    // TODO: update with real examples
    const fieldPlaceholders: string[] = [
        '1',
        '2',
        '3',
        '4',
        '5',
        '6',
        '7',
        '8',
        '...',
    ];

    return (
        <Form className="mx-auto grid max-w-lg gap-4 md:max-w-none md:auto-rows-auto md:grid-cols-[40%_1fr] md:grid-rows-[auto_1fr]">
            <div className="space-y-4">
                <div className="flex gap-4">
                    <div className="grow">
                        <LabelAbove htmlFor="templateId">Template</LabelAbove>
                        <ListboxSelect
                            {...getFieldProps('templateId')}
                            options={templates}
                            renderButton={(value: Template) => (
                                <span>
                                    <span className="mr-2">{value.emoji}</span>
                                    {value.name}
                                </span>
                            )}
                            renderOption={(option: Template) => (
                                <OptionComponent>
                                    <span className="mr-2">{option.emoji}</span>
                                    {option.name}
                                </OptionComponent>
                            )}
                        />
                    </div>
                    <div className="w-24 shrink-0">
                        <LabelAbove htmlFor="size">Size</LabelAbove>
                        <ListboxSelect
                            {...getFieldProps('size')}
                            options={sizes.map((size) => ({
                                id: size,
                                name: `${size}x${size}`,
                            }))}
                        />
                    </div>
                </div>
                <div>
                    <LabelAbove htmlFor="title">Title</LabelAbove>
                    <Input
                        {...getFieldProps('title')}
                        placeholder="Enter a title... or not"
                    />
                </div>
                <div>
                    <LabelAbove htmlFor="description">Description</LabelAbove>
                    <Input
                        {...getFieldProps('description')}
                        placeholder="...and a funny description"
                    />
                </div>
                <div className="flex justify-center gap-4">
                    <SwitchCheckbox
                        {...getFieldProps('shuffle')}
                        label="Shuffle"
                    />
                    {values.size % 2 !== 0 && (
                        <SwitchCheckbox
                            {...getFieldProps('joker')}
                            label="Joker in the middle"
                        />
                    )}
                </div>
            </div>
            <fieldset className="relative w-full grow md:col-start-2 md:row-span-2">
                {!examples && !examplesError && (
                    <>
                        <div className="absolute inset-0 z-[1] bg-white/50 dark:bg-black/50" />
                        <LoadingWrapper className="absolute inset-0 z-[1]">
                            Loading examples...
                        </LoadingWrapper>
                    </>
                )}
                <legend className="flex w-full items-center justify-between">
                    <Label htmlFor="fields">Fields</Label>
                </legend>
                <ShowMoreWrapper
                    expanded={showAllFields}
                    onToggle={() => setShowAllFields(!showAllFields)}
                    showToggleButton={values.fields.length > 9}
                >
                    <FieldArray name="fields">
                        {({ push, remove }) => (
                            <div aria-label="fields">
                                <ol className="mb-2 space-y-2">
                                    {values.fields.map((_, index) => (
                                        <FieldComponent
                                            key={index}
                                            index={index}
                                            placeholder={
                                                fieldPlaceholders[index]
                                            }
                                            getFieldProps={getFieldProps}
                                            touched={touched.fields?.[index]}
                                            errors={errors.fields?.[index]}
                                            remove={remove}
                                        />
                                    ))}
                                </ol>
                                <div className="flex items-center gap-4">
                                    <IconButton
                                        onClick={() => push({ text: '' })}
                                        IconType={PlusIcon}
                                        type="button"
                                        className="bg-gray-100 hover:bg-teal-100 dark:bg-gray-800 dark:text-white dark:hover:bg-teal-800"
                                    >
                                        Add Field
                                    </IconButton>

                                    {typeof errors.fields === 'string' && (
                                        <ErrorMessage>
                                            {errors.fields}
                                        </ErrorMessage>
                                    )}
                                </div>
                            </div>
                        )}
                    </FieldArray>
                </ShowMoreWrapper>
            </fieldset>
            <div className="mb-3 text-center">
                <SubmitButton isSubmitting={isSubmitting} IconType={PlayIcon}>
                    Play
                </SubmitButton>
            </div>
            <div className="col-span-full break-all">
                {/* <section>
                    <h2>values</h2>
                    {JSON.stringify(values)}
                </section> */}
                {/* <section>
                    <h2>touched</h2>
                    {JSON.stringify(touched)}
                </section> */}
                {/* <section>
                    <h2>dirty</h2>
                    {JSON.stringify(dirty)}
                </section> */}
                {/* <section>
                    <h2>errors</h2>
                    {JSON.stringify(errors)}
                </section> */}
            </div>
        </Form>
    );
}

function FieldComponent({
    index,
    getFieldProps,
    placeholder,
    touched,
    errors,
    remove,
}: {
    index: number;
    getFieldProps: (name: string) => FieldInputProps<string>;
    placeholder?: string;
    touched?: FormikTouched<{ text: string }>;
    errors?: string | FormikErrors<{ text: string }>;
    remove: <T>(index: number) => T | undefined;
}) {
    const name = `fields.${index}.text`;
    const label = index + 1;
    return (
        <li>
            <div className="relative flex items-center">
                <Label htmlFor={name} className="mr-1 w-6 text-right">
                    {label}.<span className="sr-only"> field</span>
                </Label>
                <Input
                    {...getFieldProps(name)}
                    placeholder={placeholder}
                    errorMessage={
                        touched?.text && typeof errors !== 'string'
                            ? errors?.text
                            : undefined
                    }
                />
                <div className="ml-1 shrink-0">
                    {/* <button
                        type="button"
                        onClick={() => index && move(index, index - 1)}
                        className="group ml-1 rounded-md p-2 hover:bg-gray-100 hover:dark:bg-gray-900"
                    >
                        <ChevronUpIcon className="h-4 w-4 text-gray-500 group-hover:text-gray-600" />
                    </button>
                    <button
                        type="button"
                        onClick={() => move(index, index + 1)}
                        className="group ml-1 rounded-md p-2 hover:bg-gray-100 hover:dark:bg-gray-900"
                    >
                        <ChevronDownIcon className="h-4 w-4 text-gray-500 group-hover:text-gray-600" />
                    </button> */}
                    <button
                        type="button"
                        onClick={() => remove(index)}
                        tabIndex={-1}
                        aria-label="Delete field"
                        className="group ml-1 rounded-md p-2 hover:bg-red-200 hover:dark:bg-red-900"
                    >
                        <TrashIcon className="h-4 w-4 fill-gray-500 group-hover:fill-red-900 dark:group-hover:fill-red-200" />
                    </button>
                </div>
            </div>
        </li>
    );
}
