import {
    DocumentSnapshot,
    FirestoreError,
    QuerySnapshot,
} from 'firebase/firestore';
import {
    useCollectionData,
    useDocumentData,
} from 'react-firebase-hooks/firestore';
import { Game } from '../pages/api/games';
import { getGameRef } from '../pages/api/games/[gameId]';
import { Field, getFieldsRef } from '../pages/api/games/[gameId]/fields';
import { getPlayersRef, Player } from '../pages/api/games/[gameId]/players';

/**
 * Hook to subscribe to a bingo game.
 *
 * Used in GamePage component.
 *
 * @param gameId game id
 * @returns list with game object, loading status, error and query snapshot
 */
export function useGame(
    gameId: string | undefined
): [
    Game | undefined,
    boolean,
    Error | undefined,
    DocumentSnapshot<Game> | undefined
] {
    const gameRef = gameId ? getGameRef(gameId) : undefined;
    const [game, loading, error, snapshot] = useDocumentData(gameRef);

    if (snapshot && !snapshot.exists()) {
        return [
            undefined,
            loading,
            Error(`Game with id ${gameId} not found.`),
            snapshot,
        ];
    }

    return [game, loading, error, snapshot];
}

export function useFields(
    gameId: string | undefined
): [
    Field[] | undefined,
    boolean,
    FirestoreError | undefined,
    QuerySnapshot<Field> | undefined
] {
    const fieldsRef = gameId ? getFieldsRef(gameId) : undefined;
    return useCollectionData(fieldsRef);
}

export function usePlayers(
    gameId: string | undefined
): [
    Player[] | undefined,
    boolean,
    FirestoreError | undefined,
    QuerySnapshot<Player> | undefined
] {
    const playersRef = gameId ? getPlayersRef(gameId) : undefined;
    return useCollectionData(playersRef);
}
