import { useEffect } from 'react';
import { useAuthState } from 'react-firebase-hooks/auth';
import { useDocumentData } from 'react-firebase-hooks/firestore';
import { useLoadingValue } from 'react-firebase-hooks/util';
import { auth } from '../firebase';
import { getUserRef, User } from '../pages/api/users';
import { setUser } from '../pages/api/users/[userId]';

export function useCurrentUser(): [
    User | null | undefined,
    boolean,
    Error | undefined
] {
    const { value, loading, error, setValue, setError } = useLoadingValue<
        User | null,
        Error
    >();
    const [authUser, authLoading, authError] = useAuthState(auth);
    const [user, userLoading, userError, snapshot] = useDocumentData(
        authUser ? getUserRef(authUser.uid) : undefined
    );

    useEffect(() => {
        if (authError) return setError(authError);
        if (userError) return setError(userError);

        if (authLoading || userLoading) return;

        if (authUser === null) return setValue(null);

        if (user) {
            setValue(user);
        } else if (snapshot && !snapshot.exists()) {
            setUser(snapshot.id);
        }
    }, [
        authUser,
        authLoading,
        authError,
        user,
        userLoading,
        userError,
        snapshot,
        setValue,
        setError,
    ]);

    return [value, loading, error];
}
