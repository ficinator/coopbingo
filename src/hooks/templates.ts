import useSWR from 'swr';
import { Example, Template } from '../pages/api/templates';

export function useTemplate(templateId?: string): {
    template: Template | undefined;
} {
    const { data: template } = useSWR<Template>(
        templateId && `/api/templates/${templateId}`,
        {
            revalidateOnFocus: false,
        }
    );
    return { template };
}

export function useExamples(templateId?: string): {
    examples: Example[] | undefined;
    error: Error | undefined;
} {
    const { data: examples, error } = useSWR<Example[]>(
        templateId && `/api/templates/${templateId}/examples`,
        {
            revalidateOnFocus: false,
        }
    );
    return { examples, error };
}
