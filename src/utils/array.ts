export function shuffle<T>(array: T[]): T[] {
    const result = Array().concat(array);
    for (let i = array.length - 1; i > 0; i--) {
        const j = Math.floor(Math.random() * (i + 1));
        [result[i], result[j]] = [result[j], result[i]];
    }
    return result;
}

export function range(end: number): number[] {
    const array = [];
    for (let i = 0; i < end; i++) {
        array.push(i);
    }
    return array;
}

export function reshape<T>(array: T[], rows: number, cols: number): T[][] {
    let matrix: T[][] = [];
    for (let i = 0; i < rows; i++) {
        let row: T[] = [];
        for (let j = 0; j < cols; j++) row.push(array[i * cols + j]);
        matrix.push(row);
    }
    return matrix;
}
