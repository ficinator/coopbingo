import { getTitle } from './text';

describe('text utils', () => {
    it.each([
        [undefined, 'Co-op Bingo'],
        ['test', 'test | Co-op Bingo'],
    ])('should get title', (title, expected) => {
        expect(getTitle(title)).toBe(expected);
    });
});
