import { Field } from '../pages/api/games/[gameId]/fields';

export type Size = 3 | 4 | 5 | 6 | 7;

export const sizes: Size[] = [3, 4, 5, 6, 7];

export const defaultGameSize: Size = 5;

export type Color =
    | 'red'
    | 'orange'
    | 'amber'
    | 'yellow'
    | 'lime'
    | 'green'
    | 'teal'
    | 'cyan'
    | 'blue'
    | 'indigo'
    | 'violet'
    | 'pink';

const colorClassNames: {
    [key in Color]: { [key in 'base' | 'from' | 'via' | 'to']: string };
} = {
    red: {
        base: 'bg-red-600 text-white',
        from: 'from-red-600 dark:from-red-600',
        via: 'via-red-600 dark:via-red-600',
        to: 'to-red-600 dark:to-red-600',
    },
    blue: {
        base: 'bg-blue-600 text-white',
        from: 'from-blue-600 dark:from-blue-600',
        via: 'via-blue-600 dark:via-blue-600',
        to: 'to-blue-600 dark:to-blue-600',
    },
    yellow: {
        base: 'bg-yellow-400 text-black',
        from: 'from-yellow-400 dark:from-yellow-400',
        via: 'via-yellow-400 dark:via-yellow-400',
        to: 'to-yellow-400 dark:to-yellow-400',
    },
    green: {
        base: 'bg-green-600 text-white',
        from: 'from-green-600 dark:from-green-600',
        via: 'via-green-600 dark:via-green-600',
        to: 'to-green-600 dark:to-green-600',
    },
    indigo: {
        base: 'bg-indigo-700 text-white',
        from: 'from-indigo-700 dark:from-indigo-700',
        via: 'via-indigo-700 dark:via-indigo-700',
        to: 'to-indigo-700 dark:to-indigo-700',
    },
    teal: {
        base: 'bg-teal-600 text-white',
        from: 'from-teal-600 dark:from-teal-600',
        via: 'via-teal-600 dark:via-teal-600',
        to: 'to-teal-600 dark:to-teal-600',
    },
    amber: {
        base: 'bg-amber-500 text-black',
        from: 'from-amber-500 dark:from-amber-500',
        via: 'via-amber-500 dark:via-amber-500',
        to: 'to-amber-500 dark:to-amber-500',
    },
    lime: {
        base: 'bg-lime-500 text-black',
        from: 'from-lime-500 dark:from-lime-500',
        via: 'via-lime-500 dark:via-lime-500',
        to: 'to-lime-500 dark:to-lime-500',
    },
    orange: {
        base: 'bg-orange-600 text-black',
        from: 'from-orange-600 dark:from-orange-600',
        via: 'via-orange-600 dark:via-orange-600',
        to: 'to-orange-600 dark:to-orange-600',
    },
    cyan: {
        base: 'bg-cyan-400 text-black',
        from: 'from-cyan-400 dark:from-cyan-400',
        via: 'via-cyan-400 dark:via-cyan-400',
        to: 'to-cyan-400 dark:to-cyan-400',
    },
    violet: {
        base: 'bg-violet-600 text-white',
        from: 'from-violet-600 dark:from-violet-600',
        via: 'via-violet-600 dark:via-violet-600',
        to: 'to-violet-600 dark:to-violet-600',
    },
    pink: {
        base: 'bg-pink-500 text-black',
        from: 'from-pink-500 dark:from-pink-500',
        via: 'via-pink-500 dark:via-pink-500',
        to: 'to-pink-500 dark:to-pink-500',
    },
};

export const colors = Object.keys(colorClassNames) as Color[];

const defaultColorClassName = {
    base: 'bg-black text-white dark:bg-white dark:text-black',
    from: 'from-black dark:from-white',
    via: 'via-black dark:via-white',
    to: 'to-black dark:to-white',
};

export function getBackgroundClassName(colors: (Color | undefined)[]): string {
    const classNames = colors.map((color) =>
        color ? colorClassNames[color] : defaultColorClassName
    );

    switch (classNames.length) {
        case 0:
            return defaultColorClassName.base;
        case 1:
            return classNames[0].base;
        default:
            return `bg-gradient-to-tr text-white dark:text-black ${
                classNames[0].from
            } ${
                classNames.length === 2
                    ? classNames[1].to
                    : `${classNames[1].via} ${classNames[2].to}`
            }`;
    }
}

export function getFields(
    size: number,
    examples?: { text: string }[]
): { text: string }[] {
    const numFields = Math.max(size * size, examples?.length || 0);
    const newFields = [];
    for (let i = 0; i < numFields; i++) {
        newFields.push({ text: examples?.[i]?.text || '' });
    }
    // Array.from({ length: numFields }).map(
    //     (_, index: number) => ({
    //         text: examples?.[index]?.text || '',
    //     })
    // );
    return newFields;
}

export function getSelectedFieldCount(fields: Field[], userId: string): number {
    const count = fields.filter((field) =>
        (field.selectedByUsers || []).includes(userId)
    ).length;
    return count;
}

export function checkWin(size: number, fields: Field[] | undefined): boolean {
    if (fields === undefined) {
        return false;
    }

    let numSelected = 0;

    // horizontal
    for (let row = 0; row < size; row++) {
        numSelected = 0;
        for (let col = 0; col < size; col++) {
            numSelected += isFieldSelected(fields[row * size + col]) ? 1 : 0;
        }
        if (numSelected === size) return true;
    }

    // vertical
    for (let col = 0; col < size; col++) {
        numSelected = 0;
        for (let row = 0; row < size; row++) {
            numSelected += isFieldSelected(fields[row * size + col]) ? 1 : 0;
        }
        if (numSelected === size) return true;
    }

    // diagonals
    numSelected = 0;
    for (let i = 0; i < size * size; i += size + 1) {
        numSelected += isFieldSelected(fields[i]) ? 1 : 0;
    }
    if (numSelected === size) return true;

    numSelected = 0;
    for (let i = size - 1; i <= size * (size - 1); i += size - 1) {
        numSelected += isFieldSelected(fields[i]) ? 1 : 0;
    }
    if (numSelected === size) return true;

    return false;
}

export function isFieldSelected(field: Field): boolean {
    return field.isJoker || (field.selectedByUsers || []).length > 0;
}
