import { range, reshape, shuffle } from './array';

describe('array utils', () => {
    it.each([
        [[]],
        [[1]],
        [[1, 2]],
        [[1, 2, 3, 4, 5]],
        [
            [
                { id: '1', name: 't1' },
                { id: '2', name: 't2' },
                { id: '3', name: 't3' },
            ],
        ],
    ])('shuffle array %s', (inputArray: any[]) => {
        const inputArrayCopy = inputArray.slice();
        const outputArray = shuffle(inputArray);

        expect(inputArray).toStrictEqual(inputArrayCopy);
        expect(new Set(inputArray)).toStrictEqual(new Set(outputArray));
    });

    it.each([
        [0, []],
        [1, [0]],
        [2, [0, 1]],
        [-1, []],
    ])('array range %s', (end: number, expectedArray) => {
        expect(range(end)).toStrictEqual(expectedArray);
    });

    it.each([
        [[], 0, 0, []],
        [[], 1, 1, [[undefined]]],
        [[], 1, 2, [[undefined, undefined]]],
        [[], 2, 1, [[undefined], [undefined]]],
        [[1], 1, 2, [[1, undefined]]],
        [[1], 2, 1, [[1], [undefined]]],
        [
            [1, 2],
            2,
            2,
            [
                [1, 2],
                [undefined, undefined],
            ],
        ],
        [
            [1, 2, 3, 4, 5],
            2,
            2,
            [
                [1, 2],
                [3, 4],
            ],
        ],
        [
            [1, 2, 3, 4, 5],
            2,
            3,
            [
                [1, 2, 3],
                [4, 5, undefined],
            ],
        ],
    ])('reshape array %s to %sx%s', (array, rows, cols, expectedArray) => {
        expect(reshape(array, rows, cols)).toStrictEqual(expectedArray);
    });
});
