export const appName = 'Co-op Bingo';

export function getTitle(title?: string): string {
    return (title ? `${title} | ` : '') + appName
}
