import { ReactNode } from 'react';
import { Spinner } from './spinner';

export function LoadingWrapper({
    className,
    children,
}: {
    className?: string;
    children: ReactNode;
}) {
    return (
        <div
            className={`m-2 flex items-center justify-center ${
                className || ''
            }`}
        >
            <Spinner className="mr-2 h-5 w-5" />
            {children}
        </div>
    );
}
