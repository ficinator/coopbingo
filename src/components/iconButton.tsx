import {
    ButtonHTMLAttributes,
    ComponentProps,
    ComponentType,
    DetailedHTMLProps,
} from 'react';

export type IconProps = ComponentProps<'svg'> & {
    title?: string;
    titleId?: string;
};

export type IconButtonProps = DetailedHTMLProps<
    ButtonHTMLAttributes<HTMLButtonElement>,
    HTMLButtonElement
> & {
    IconType?: ComponentType<IconProps>;
};

export function IconButton({
    className,
    IconType,
    children,
    ...props
}: IconButtonProps) {
    return (
        <button
            {...props}
            className={`inline-flex items-center rounded-md px-3 py-2 disabled:cursor-not-allowed disabled:bg-gray-400 ${
                className || ''
            }`}
        >
            {IconType && <IconType className="mr-2 h-4 w-4" />}
            {children}
        </button>
    );
}
