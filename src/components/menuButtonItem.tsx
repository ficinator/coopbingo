import classNames from 'classnames';
import { IconButton, IconButtonProps } from './iconButton';

export function MenuButtonItem(props: IconButtonProps) {
    const { IconType } = props;
    return (
        <IconButton
            {...props}
            className={classNames({
                'w-full': true,
                'pl-8': !IconType,
            })}
        ></IconButton>
    );
}
