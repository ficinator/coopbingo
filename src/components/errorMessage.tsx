import { ReactNode } from 'react';

export function ErrorMessage({
    className,
    children,
}: {
    className?: string;
    children: ReactNode;
}) {
    return (
        <span className={`text-sm text-red-500 ${className || ''}`}>
            {children}
        </span>
    );
}
