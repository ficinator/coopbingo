import { CheckIcon } from '@heroicons/react/24/solid';
import { IconButton, IconButtonProps } from './iconButton';
import { Spinner } from './spinner';

export function SubmitButton({
    isSubmitting,
    IconType,
    children,
    ...props
}: {
    isSubmitting?: boolean;
} & IconButtonProps) {
    const SubmitIconType = IconType || CheckIcon;
    return (
        <IconButton
            {...props}
            type="submit"
            disabled={isSubmitting}
            IconType={isSubmitting ? Spinner : SubmitIconType}
            className="bg-primary text-white hover:bg-primary-light"
        >
            {children}
        </IconButton>
    );
}
