import { ReactNode } from 'react';

type Props = {
    htmlFor: string;
    className?: string;
    children: ReactNode;
};

export function Label({ htmlFor, className, children }: Props) {
    return (
        <label
            htmlFor={htmlFor}
            className={`text-gray-500 dark:text-gray-400 ${className || ''}`}
        >
            {children}
        </label>
    );
}

export function LabelAbove(props: Props) {
    return (
        <Label
            {...props}
            className={`mb-1 block ${props.className || ''}`}
        ></Label>
    );
}
