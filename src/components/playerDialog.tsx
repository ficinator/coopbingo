import { CheckIcon, XMarkIcon } from '@heroicons/react/24/solid';
import { Form, Formik, FormikProps } from 'formik';
import * as Yup from 'yup';
import { Game } from '../pages/api/games';
import { addPlayer, Player } from '../pages/api/games/[gameId]/players';
import { updatePlayer } from '../pages/api/games/[gameId]/players/[playerId]';
import { User } from '../pages/api/users';
import { updateUser } from '../pages/api/users/[userId]';
import { Color, colors, getBackgroundClassName } from '../utils/game';
import { BaseDialog } from './baseDialog';
import { IconButton } from './iconButton';
import { Input } from './input';
import { ListboxSelect } from './listbox';
import { SubmitButton } from './submitButton';

type Values = {
    name: string;
    color: Color;
};

export function PlayerDialog({
    open,
    onClose,
    game,
    currentUser,
    players,
}: {
    open: boolean;
    onClose: () => void;
    game: Game;
    currentUser: User;
    players: Player[] | undefined;
}) {
    const gameId = game.id;
    const currentUserId = currentUser.id;
    const currentPlayer = players?.find(
        (player) => player.userId === currentUserId
    );
    const usedColors = players
        ?.filter((player) => player.userId !== currentUserId)
        .map((player) => player.color);
    const availableColors = colors.filter(
        (color) => !usedColors?.includes(color)
    );

    const initialValues: Values = {
        name: currentPlayer?.name || currentUser.name || '',
        color: currentPlayer?.color || availableColors[0],
    };

    const validationSchema = Yup.object({
        name: Yup.string().required('name is required'),
    });

    const onSubmit = async ({ name, color }: Values) => {
        const userPromise = updateUser(currentUserId, { name });
        const player = {
            userId: currentUserId,
            name,
            color,
        };
        const playerPromise = currentPlayer
            ? updatePlayer(gameId, currentPlayer.id, player)
            : addPlayer(gameId, player);
        const [_, playerId] = await Promise.all([userPromise, playerPromise]);
        console.log(
            playerId
                ? `Player ${playerId} added to game ${gameId}`
                : `Player ${currentPlayer?.id} updated.`
        );
        onClose();
    };

    return (
        <Formik
            initialValues={initialValues}
            validationSchema={validationSchema}
            onSubmit={onSubmit}
        >
            {(props) => (
                <PlayerForm
                    open={open}
                    onClose={onClose}
                    isOwner={game.ownerId === currentUserId}
                    currentPlayer={currentPlayer}
                    availableColors={availableColors}
                    {...props}
                />
            )}
        </Formik>
    );
}

function PlayerForm({
    open,
    onClose,
    isOwner,
    currentPlayer,
    availableColors,
    touched,
    errors,
    isSubmitting,
    getFieldProps,
}: {
    open: boolean;
    onClose: () => void;
    isOwner: boolean;
    currentPlayer: Player | undefined;
    availableColors: Color[];
} & FormikProps<Values>) {
    return (
        <BaseDialog
            open={open}
            onClose={currentPlayer ? onClose : () => {}}
            title={
                currentPlayer
                    ? 'Edit player'
                    : isOwner
                    ? "What's your name?"
                    : `You\'ve been invited to join a bingo game`
            }
        >
            <Form className="space-y-3">
                <div className="flex items-center gap-x-3">
                    <Input
                        {...getFieldProps('name')}
                        placeholder="Cool Name"
                        errorMessage={touched.name ? errors.name : undefined}
                    />
                    <ListboxSelect
                        {...getFieldProps('color')}
                        options={availableColors.map((color) => ({
                            id: color,
                            name: color,
                        }))}
                        renderButton={(value) => (
                            <div
                                className={`h-5 w-5 rounded-full ${getBackgroundClassName(
                                    [value.id]
                                )}`}
                            >
                                <span className="sr-only">{value.name}</span>
                            </div>
                        )}
                        renderOption={(option) => (
                            <div className="rounded-md p-2 ui-active:bg-teal-100 dark:ui-active:bg-primary">
                                <div
                                    className={`h-5 w-5 rounded-full p-1 ${getBackgroundClassName(
                                        [option.id]
                                    )}`}
                                >
                                    <CheckIcon className="hidden h-3 w-3 ui-selected:block" />
                                    <span className="sr-only">
                                        {option.name}
                                    </span>
                                </div>
                            </div>
                        )}
                        className="py-1.5"
                    />
                </div>
                <div className="flex items-center justify-center gap-3">
                    <SubmitButton isSubmitting={isSubmitting}>
                        Save
                    </SubmitButton>

                    {currentPlayer && (
                        <IconButton
                            onClick={onClose}
                            IconType={XMarkIcon}
                            className="bg-gray-300 pl-2 pr-3 hover:bg-gray-200 dark:bg-gray-600 dark:text-white dark:hover:bg-gray-500"
                        >
                            Cancel
                        </IconButton>
                    )}
                </div>
            </Form>
            {currentPlayer && (
                <button
                    onClick={onClose}
                    className="absolute top-0 right-0 p-2"
                >
                    <XMarkIcon className="h-4 w-4" aria-label="Close" />
                </button>
            )}
        </BaseDialog>
    );
}
