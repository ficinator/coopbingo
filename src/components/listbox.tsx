import { Listbox } from '@headlessui/react';
import { ChevronUpDownIcon } from '@heroicons/react/24/solid';
import { FieldInputProps } from 'formik';
import { ReactNode } from 'react';
import { Option, OptionComponent } from './option';

type Props<TOption extends Option> = FieldInputProps<string | number> & {
    options: TOption[];
    renderButton?: (value: TOption) => ReactNode;
    renderOption?: (option: TOption) => ReactNode;
    className?: string;
};

export function ListboxSelect<TOption extends Option>({
    name,
    value,
    onChange,
    options,
    renderButton,
    renderOption,
    className,
}: Props<TOption>) {
    const selectedOption = options.find((option) => option.id === value);

    return (
        <Listbox
            value={value}
            onChange={(value: string | number) =>
                onChange({ target: { value, name } })
            }
        >
            <div className="relative">
                <Listbox.Button
                    id={name}
                    className={`inline-flex w-full items-center justify-between rounded-md border border-gray-300 px-2 leading-8 hover:bg-gray-200 dark:border-gray-600 dark:bg-gray-800 dark:hover:bg-gray-600 ${
                        className || ''
                    }`}
                    // rounded-r-md border border-l-0 p-3.5
                >
                    {renderButton && selectedOption
                        ? renderButton(selectedOption)
                        : selectedOption?.name}
                    <ChevronUpDownIcon className="ml-2 h-4 w-4" />
                </Listbox.Button>
                <Listbox.Options className="absolute z-[1] mt-1 max-h-56 w-full overflow-y-auto rounded-md bg-white p-1 shadow-lg focus:outline-none dark:bg-gray-800">
                    {options.map((option) => (
                        <Listbox.Option key={option.id} value={option.id}>
                            {renderOption ? (
                                renderOption(option)
                            ) : (
                                <OptionComponent>{option.name}</OptionComponent>
                            )}
                        </Listbox.Option>
                    ))}
                </Listbox.Options>
            </div>
        </Listbox>
    );
}

// export function Select({
//     name,
//     register,
//     options,
//     children,
// }: {
//     name: string;
//     register: UseFormRegister<any>;
//     options?: RegisterOptions;
//     children: ReactNode;
// }) {
//     return (
//         <select
//             {...register(name, options)}
//             className="h-8 w-full rounded-md border px-2 dark:border-gray-600 dark:bg-gray-800"
//         >
//             {children}
//         </select>
//     );
// }

// export function SelectWithLabel({
//     name,
//     label,
//     register,
//     options,
//     children,
//     className,
// }: {
//     name: string;
//     label?: string;
//     register: UseFormRegister<any>;
//     options?: RegisterOptions;
//     children: ReactNode;
//     className?: string;
// }) {
//     return (
//         <div className={className || ''}>
//             {label && (
//                 <label htmlFor={name} className="mb-1 block">
//                     {label}
//                 </label>
//             )}
//             <Select name={name} register={register} options={options}>
//                 {children}
//             </Select>
//         </div>
//     );
// }
