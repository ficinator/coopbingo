import { ChevronDownIcon, ChevronUpIcon } from '@heroicons/react/24/solid';

export function ShowMoreButton({
    expanded,
    onClick,
}: {
    expanded: boolean;
    onClick: () => void;
}) {
    return (
        <button
            type="button"
            onClick={onClick}
            className="inline-flex items-center text-gray-500 hover:text-black dark:text-gray-300 dark:hover:text-white"
        >
            Show {expanded ? 'Less' : 'More'}
            {expanded ? (
                <ChevronUpIcon className="ml-1 h-4 w-4" />
            ) : (
                <ChevronDownIcon className="ml-1 h-4 w-4" />
            )}
        </button>
    );
}
