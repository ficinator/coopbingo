import classNames from 'classnames';
import { FieldInputProps } from 'formik';
import { InputHTMLAttributes } from 'react';
import { ErrorMessage } from './errorMessage';
import { Label } from './label';

type Props = {
    placeholder?: string;
    className?: string;
    errorMessage?: string;
} & FieldInputProps<string> &
    InputHTMLAttributes<HTMLInputElement>;

export function Input({ name, className, errorMessage, ...props }: Props) {
    return (
        <div className="relative w-full">
            <input
                {...props}
                id={name}
                className={classNames({
                    [`w-full rounded-md border px-2 leading-8 dark:bg-gray-800 ${
                        className || ''
                    }`]: true,
                    'border-gray-300 dark:border-gray-600': !errorMessage,
                    'border-red-500 dark:border-red-500': errorMessage,
                })}
            />
            {errorMessage && (
                <ErrorMessage className="absolute right-0 top-0 mx-2 leading-8">
                    {errorMessage}
                </ErrorMessage>
            )}
        </div>
    );
}

export function InputWithLabel(props: Props & { label: string }) {
    const { label, name } = props;
    return (
        <div className="relative sm:w-full">
            {label && <Label htmlFor={name}>{label}</Label>}
            <Input {...props} />
        </div>
    );
}
