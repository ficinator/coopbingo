import { Switch } from '@headlessui/react';
import classNames from 'classnames';
import { FieldInputProps } from 'formik';
import { Label } from './label';

type Props = FieldInputProps<boolean> & {
    label: string;
};

export function SwitchCheckbox({ name, value, onChange, label }: Props) {
    return (
        <Switch
            id={name}
            checked={value}
            onChange={(value: boolean) => onChange({ target: { value, name } })}
        >
            {({ checked }) => (
                <span className="group inline-flex items-center">
                    <span
                        className={classNames({
                            'mr-1.5 inline-flex h-6 w-11 shrink-0 items-center rounded-full':
                                true,
                            'bg-primary group-hover:bg-primary-light': checked,
                            'bg-gray-200 group-hover:bg-gray-300 dark:bg-gray-700 dark:group-hover:bg-gray-600':
                                !checked,
                        })}
                    >
                        <span className="inline-block h-4 w-4 translate-x-1 transform rounded-full bg-white transition ui-checked:translate-x-6" />
                    </span>
                    <Label htmlFor={name} className="cursor-pointer">
                        {label}
                    </Label>
                </span>
            )}
        </Switch>
    );
}
