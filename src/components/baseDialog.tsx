import { Dialog } from '@headlessui/react';
import { ReactNode } from 'react';

export function BaseDialog({
    open,
    onClose,
    title,
    children,
}: {
    open: boolean;
    onClose: () => void;
    title: string;
    children: ReactNode;
}) {
    return (
        <Dialog open={open} onClose={onClose}>
            <Dialog.Overlay className="fixed inset-0 z-[1] bg-black/50" />

            <div className="fixed inset-0 z-[1] flex items-center justify-center p-4">
                <Dialog.Panel className="relative rounded-md bg-white p-4 shadow-lg dark:bg-gray-800">
                    <Dialog.Title className="mb-3">{title}</Dialog.Title>
                    {children}
                </Dialog.Panel>
            </div>
        </Dialog>
    );
}
