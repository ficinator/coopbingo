import classNames from 'classnames';
import { ReactNode } from 'react';
import { ShowMoreButton } from './showMoreButton';

export function ShowMoreWrapper({
    expanded,
    onToggle,
    showToggleButton = true,
    children,
}: {
    expanded: boolean;
    onToggle: () => void;
    showToggleButton?: boolean;
    children: ReactNode;
}) {
    return (
        <>
            <div
                className={classNames({
                    'py-1': true,
                    'max-h-60 overflow-y-hidden': showToggleButton && !expanded,
                })}
            >
                {children}
            </div>
            {showToggleButton && (
                <div
                    className={`absolute text-center ${
                        expanded
                            ? 'right-0 bottom-3'
                            : 'absolute inset-x-0 bottom-0 bg-gradient-to-b from-transparent to-white p-3 pt-20 dark:to-black'
                    }`}
                >
                    <ShowMoreButton expanded={expanded} onClick={onToggle} />
                </div>
            )}
        </>
    );
}
