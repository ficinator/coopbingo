import { render } from '@testing-library/react';
import { Label } from './label';

describe('Label', () => {
    it('should render label without children', () => {
        const htmlFor = 'test';
        const label = 'Test';
        const { getByText, getByRole } = render(
            <Label htmlFor={htmlFor}>{label}</Label>
        );

        expect(getByText(label)).toHaveAttribute('for', htmlFor);
    });
});
