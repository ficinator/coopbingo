import { Combobox } from '@headlessui/react';
import { ChevronUpDownIcon } from '@heroicons/react/24/solid';
import { FieldInputProps } from 'formik';
import { useState } from 'react';
import { Option, OptionComponent } from './option';

type Props<TOption extends Option> = FieldInputProps<TOption> & {
    options: TOption[];
};

export function ComboboxInput<TOption extends Option>(props: Props<TOption>) {
    const { name, value, onChange, options } = props;
    const [query, setQuery] = useState('');
    const filteredOptions =
        query === ''
            ? options
            : options.filter((option) =>
                  option.name.toLowerCase().includes(query)
              );
    ``;

    return (
        <Combobox
            name={name}
            value={value}
            onChange={(value: TOption) => onChange({ target: { value, name } })}
        >
            <div className="relative">
                <div className="relative">
                    <Combobox.Input
                        id={name}
                        onChange={(event) =>
                            setQuery(event.target.value.toLowerCase())
                        }
                        displayValue={(option: TOption | null) =>
                            option?.name || 'None'
                        }
                        className="w-full rounded-md border border-gray-300 pl-2 pr-8 leading-8 dark:border-gray-600 dark:bg-gray-800"
                    />
                    <Combobox.Button
                        aria-label="Toggle Options List"
                        className="absolute inset-y-0 right-0 rounded-md px-2 hover:bg-gray-100 dark:hover:bg-gray-700"
                    >
                        <ChevronUpDownIcon className="h-4 w-4" />
                    </Combobox.Button>
                </div>
                <Combobox.Options className="absolute z-[1] mt-1 w-full rounded-md bg-white p-1 shadow-lg dark:bg-gray-800">
                    {filteredOptions.length ? (
                        filteredOptions.map((option) => (
                            <Combobox.Option key={option.id} value={option}>
                                <OptionComponent>{option.name}</OptionComponent>
                            </Combobox.Option>
                        ))
                    ) : (
                        <div className="p-2">No templates</div>
                    )}
                </Combobox.Options>
            </div>
        </Combobox>
    );
}
