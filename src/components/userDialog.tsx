import { XMarkIcon } from '@heroicons/react/24/solid';
import { Form, Formik, FormikProps } from 'formik';
import * as Yup from 'yup';
import { User } from '../pages/api/users';
import { updateUser } from '../pages/api/users/[userId]';
import { BaseDialog } from './baseDialog';
import { IconButton } from './iconButton';
import { Input } from './input';
import { SubmitButton } from './submitButton';

type Values = {
    name: string;
};

export function UserDialog({
    open,
    onClose,
    currentUser,
}: {
    open: boolean;
    onClose: () => void;
    currentUser: User;
}) {
    const currentUserId = currentUser.id;

    const initialValues: Values = {
        name: currentUser.name || '',
    };

    const validationSchema = Yup.object({
        name: Yup.string().required('name is required'),
    });

    const onSubmit = async ({ name }: Values) => {
        await updateUser(currentUserId, { name });
        console.log(`User ${currentUserId} updated.`);
        onClose();
    };

    return (
        <Formik
            initialValues={initialValues}
            validationSchema={validationSchema}
            onSubmit={onSubmit}
        >
            {(props) => <UserForm open={open} onClose={onClose} {...props} />}
        </Formik>
    );
}

function UserForm({
    open,
    onClose,
    touched,
    errors,
    isSubmitting,
    getFieldProps,
}: {
    open: boolean;
    onClose: () => void;
} & FormikProps<Values>) {
    return (
        <BaseDialog open={open} onClose={onClose} title="Edit user">
            <Form className="space-y-3">
                <Input
                    {...getFieldProps('name')}
                    placeholder="Cool Name"
                    errorMessage={touched.name ? errors.name : undefined}
                />
                <div className="flex items-center justify-center gap-3">
                    <SubmitButton isSubmitting={isSubmitting}>
                        Save
                    </SubmitButton>
                    <IconButton
                        onClick={onClose}
                        IconType={XMarkIcon}
                        className="bg-gray-300 hover:bg-gray-200 dark:bg-gray-600 dark:text-white dark:hover:bg-gray-500"
                    >
                        Cancel
                    </IconButton>
                </div>
            </Form>
            <button onClick={onClose} className="absolute top-0 right-0 p-2">
                <XMarkIcon className="h-4 w-4" aria-label="Close" />
            </button>
        </BaseDialog>
    );
}
