import { CheckIcon } from '@heroicons/react/24/solid';
import { ReactNode } from 'react';

export type Option = {
    id: string | number;
    name: string;
};

export function OptionComponent({ children }: { children: ReactNode }) {
    return (
        <div className="flex cursor-pointer items-center rounded-md p-2 pl-8 ui-selected:pl-2 ui-active:bg-teal-100 dark:ui-active:bg-primary dark:ui-active:text-white">
            <CheckIcon className="mr-2 hidden h-4 w-4 ui-selected:block" />
            {children}
        </div>
    );
}
