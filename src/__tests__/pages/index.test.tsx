import Home from '@/pages';
import { render } from '@testing-library/react';

describe('Home', () => {
    it('renders the game form', async () => {
        const { getByLabelText, getAllByLabelText } = render(
            <Home templates={[]} />
        );

        const comboboxElem = getByLabelText('Template');
        expect(comboboxElem).toHaveTextContent('');

        const sizeSelectElem = getByLabelText('Size');
        expect(sizeSelectElem).toHaveTextContent('5x5');

        const titleInputElem = getByLabelText('Title');
        expect(titleInputElem).toHaveValue('');

        const fieldInputElems = getAllByLabelText(/\d+\. field/);
        expect(fieldInputElems.length).toBe(25);
        fieldInputElems.forEach((fieldInputElem) =>
            expect(fieldInputElem).toHaveTextContent('')
        );
    });
});
