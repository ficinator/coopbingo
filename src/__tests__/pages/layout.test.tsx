import Layout from '@/pages/layout';
import { render, screen } from '@testing-library/react';

describe('Layout', () => {
    it('layout without props', () => {
        render(<Layout>Test</Layout>);

        const appNameElem = screen.getByText('Co-op Bingo', {
            selector: 'span',
        });
        expect(appNameElem).toBeDefined();

        const userElem = screen.queryByText('???');
        expect(userElem).toBeDefined();
    });

    it('layout with title prop', () => {
        const title = 'Test';
        render(<Layout title={title}>Test</Layout>);

        // TODO: find out how to test the title in head element
    });

    it('layout with user with name', () => {
        const name = 'TestName';
        const user = { id: 'testUid', name };
        render(<Layout currentUser={user}>Test</Layout>);

        const userElem = screen.queryByText(name);
        expect(userElem).toBeDefined();
    });
});
