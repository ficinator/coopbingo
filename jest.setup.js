// Optional: configure or set up a testing framework before each test.
// If you delete this file, remove `setupFilesAfterEnv` from `jest.config.js`

// Used for __tests__/testing-library.js
// Learn more: https://github.com/testing-library/jest-dom
import '@testing-library/jest-dom/extend-expect';

// TODO: should global mocks be somewhere else?
jest.mock('next/router', () => ({
    useRouter: jest.fn(),
}));

jest.mock('firebase/auth', () => ({
    getAuth: jest.fn(),
}));

jest.mock('react-firebase-hooks/auth', () => ({
    useAuthState: jest.fn(() => [{ uid: 'testUid' }, false, undefined]),
}));
