/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: true,
  swcMinify: true,
  // // for next export, remove when running a node.js server
  // images: {
  //   unoptimized: true,
  // },
  // // since the root in gitlab pages is not in / but /coopbingo
  // basePath: process.env.NODE_ENV === 'production' ? '/coopbingo' : '',
}

module.exports = nextConfig
