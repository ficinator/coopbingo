const colors = require('tailwindcss/colors');

/** @type {import('tailwindcss').Config} */
module.exports = {
    content: [
        'src/pages/**/*.{ts,tsx}',
        'src/components/**/*.{ts,tsx}',
        'src/utils/**/*.ts',
    ],
    theme: {
        extend: {
            colors: {
                primary: {
                    DEFAULT: colors.teal[700],
                    light: colors.teal[600],
                    dark: colors.teal[800],
                },
            },
        },
    },
    plugins: [require('@headlessui/tailwindcss')],
    // darkMode: 'class',
};
